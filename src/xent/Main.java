package xent;

import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

import xent.checkers.Checker;
import xent.checkers.Compare;
import xent.checkers.CorregirAutor;
import xent.checkers.Check_iTMD;
import xent.checkers.MergerEpubs;
import xent.checkers.NormalizeText;
import xent.checkers.PList2OPF;
import xent.checkers.RemoveSorts_iTMD;
import xent.checkers.Zipper;

public class Main {

	public static void main(String args[]) {

		instatiateCheckers();
		if (args.length < 2) {
			System.out.println("Must specify dir and checker's list.");
			showCheckers();
			System.exit(0);
		}

		File root = loadArg1(args);

		Vector<Checker> chks = loadArg2(args);

		Recurse r = new Recurse(root);

		r.proceed(chks);
	}

	private static Vector<Checker> loadArg2(String[] args) {
		// Buscamos los checkers
		Vector<Checker> chks = new Vector<Checker>();
		for (int i = 1; i < args.length; i++) {
			Checker k = Checker.checkersList.get(args[i]);
			if (k == null) {
				System.out.println(args[i] + " is not a valid Checker");
				System.exit(-2);
			} else {
				Vector<String> argumentos = new Vector<String>();
				try {
					for (int j = 0; j < k.getNumberOfArgNeeded(); j++) {
						argumentos.add(args[++i]);
					}
				} catch (IndexOutOfBoundsException e) {
					System.err.println("Insufficent number of arguments!");
					System.err.println(k.getDescription());
					System.exit(-1);
				}
				k.setArgs(argumentos);
				chks.add(k);
			}
		}
		return chks;
	}

	private static File loadArg1(String[] args) {
		// Buscamos los libros
		File root = new File(args[0]);
		if (!root.isDirectory()) {
			System.out.println((new StringBuilder("'").append(String
					.valueOf(root.getAbsolutePath()))).append(
					" must be a directory").toString());
			System.exit(0);
		}
		return root;
	}

	private static void showCheckers() {
		System.out.println("Checkers are:\n-------------\n");
		for (Enumeration<String> e = Checker.getList().keys(); e
				.hasMoreElements();) {
			String nombre = e.nextElement();
			System.out.println(nombre + ":\n");
			System.out.println(Checker.getList().get(nombre).getDescription());
			System.out.println("");
		}
	}

	private static void instatiateCheckers() {
		new CorregirAutor();
		new NormalizeText();
		new Zipper();
		new PList2OPF();
		new Check_iTMD();
		new RemoveSorts_iTMD();
		new Compare();
		new MergerEpubs();
	}
}
