package xent;

public class SameLineProgress {
	
	String spaces="          ";
	String sameLineList=(System.getProperty("multipleListLog")!=null)?"\n":"";
	int problemSize,lineSize;
	String staticMsg;
	
	public SameLineProgress(int lineSz,int sz, String msg){
		lineSize=lineSz;
		while(spaces.length()<lineSize)
			spaces+=spaces;
		spaces=spaces.substring(0,lineSize);
		problemSize=sz;
		staticMsg=msg;
	}
	
	public void report(int avance,String msg){
		System.out.print(leftMsg(avance, msg));
	}

	public void report(int avance, String msg, String rightMsg){
		String left=leftMsg(avance, msg);
		while(rightMsg.length()>left.length())
			rightMsg=rightMsg.substring(2);
		left=left.substring(0,lineSize-rightMsg.length())+rightMsg;
		System.out.print(left);
	}

	private String leftMsg(int avance, String msg) {
		int pc=((avance)*100)/problemSize;
		String show=String.format("\r%02d%%: "+staticMsg+" "+msg, pc);
		while(show.length()<lineSize)
			show+=spaces;
		return show.substring(0,lineSize);
	}
	
}
