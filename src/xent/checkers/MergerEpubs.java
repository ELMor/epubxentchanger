package xent.checkers;

import java.io.File;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import xent.SameLineProgress;
import xent.epub.Epub;

public class MergerEpubs extends Checker {
	
	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	Hashtable<String, Vector<File>> auts=new Hashtable<String, Vector<File>>();
	
	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean exec(Epub epub) throws Exception {
		Vector<File> books=auts.get(epub.getAutor());
		if(books==null)
			books=new Vector<File>();
		books.add(epub.nameOfFile);
		auts.put(epub.getAutor(), books);
		return false;
	}

	@Override
	public String getMsgIsNotModified() {
		return "Indexed";
	}

	@Override
	public boolean finalizar() throws Exception {
		Set<String> autores=auts.keySet();
		SameLineProgress slp=new SameLineProgress(79, autores.size(), "Merging");
		int numAut=1;
		for(String aut:autores){
			slp.report(numAut++, aut);
			new Epub(new File(aut+"-"+tit+".epub"),auts.get(aut));
		}
		return true;
	}

	@Override
	public String getMsgChecking() {
		return "Indexing";
	}

	@Override
	public String getMsgIsModified() {
		return null;
	}

	@Override
	public String getDescription() {
		return "Pack books into one by author";
	}

	@Override
	public int getNumberOfArgNeeded() {
		return 1;
	}

	String tit;
	@Override
	public void setArgs(Vector<String> arg) {
		tit=arg.elementAt(0);
		
	}
}
