package xent.checkers;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import lrf.Utils;
import xent.epub.Epub;
import xent.epub.iTunesMetadata;

public class Check_iTMD extends Checker {

	@Override
	public boolean initialize() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean exec(Epub epub) throws Exception {
		String key="iTunesMetadata.plist";
		ByteArrayOutputStream itmd=epub.getZFC().get(key);
		String txt="", lin="";
		BufferedReader br;
		if(itmd!=null){
			String contenido=itmd.toString();
			br=new BufferedReader(new StringReader(contenido));
		}else{
			InputStream istream=getClass().getResourceAsStream(key);
			br=new BufferedReader(new InputStreamReader(istream));
		}
		while((lin=br.readLine())!=null)
			txt+=lin;
		txt=txt.replace("[1]", epub.content.getAutor_epub());
		txt=txt.replace("[2]", epub.content.getTitulo_epub());
		txt=Utils.toUnhandText(txt);
		epub.itunes=new iTunesMetadata(txt);
		epub.getZFC().put(key, epub.itunes);
		return true;
	}

	@Override
	public boolean finalizar() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getMsgIsModified() {
		return "Created iTunesMetadata";
	}

	@Override
	public String getMsgChecking() {
		return "Checking iTunesMetadata.plist";
	}

	@Override
	public String getDescription() {
		return "Checks iTunesMetadata.plist and creates it if not exists.";
	}

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

}
