package xent.checkers;

import java.util.Hashtable;
import java.util.Vector;

import xent.epub.Epub;

public abstract class Checker {
	/**
	 * Realiza la comprobación con el epub.
	 * @param epub Libro sobre el que trabajar
	 * @return true si se ha producido un cambio en el libro que haya que guardar (Epub.save());
	 */
	public static Hashtable<String, Checker> checkersList=new Hashtable<String, Checker>();  

	public static Hashtable<String, Checker> getList(){ 
		return checkersList;
	}
	public Checker(){
		checkersList.put(getClass().getName().substring(14), this);
	}
	
	/**
	 * Called first time only one
	 * @return true if all OK. False causes exception.
	 * @throws Exception
	 */
	public abstract boolean initialize() throws Exception;
	/**
	 * Called each time a book is found on main dir
	 * @param epub Book found
	 * @return true if it has been modified
	 * @throws Exception
	 */
	public abstract boolean exec(Epub epub) throws Exception;
	public abstract boolean finalizar() throws Exception;
	
	public abstract Class<?> getClassToProcess();
	
	public abstract String getMsgChecking();
	
	public abstract String getMsgIsModified();

	public String getMsgIsNotModified(){
		return "Untouched";
	}
	
	public abstract String getDescription();
	
	public int getNumberOfArgNeeded(){
		return 0;
	}
	protected Vector<String> argumentos=null;
	public void setArgs(Vector<String> arg){
		argumentos=arg;
	}
}
