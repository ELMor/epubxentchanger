package xent.checkers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import xent.Recurse;
import xent.SameLineProgress;
import xent.epub.Epub;

public class Compare extends Checker {
	ZipOutputStream zNewTitles;
	ZipOutputStream zNewVersions;

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	@Override
	public int getNumberOfArgNeeded() {
		return 1;
	}

	Vector<EpubNames> epubsExisting = new Vector<Compare.EpubNames>();

	int existingZFNNumber = 0, newsZFNNumber = 0;
	Vector<File> candidateList = new Vector<File>();

	@Override
	public boolean initialize() throws Exception {
		File dir = new File(argumentos.get(0));
		Recurse.recurse(dir, candidateList);
		System.out.println("Checking for new additions from "+candidateList.size() + " candidates.");
		if (candidateList.size() == 0)
			throw new RuntimeException("No epub files to check!");
		zNewTitles = new ZipOutputStream(new FileOutputStream(new File("NewTitles.zip")));
		zNewVersions = new ZipOutputStream(new FileOutputStream(new File("NewVersions.zip")));
		return true;
	}

	int existing = 0;

	@Override
	public boolean exec(Epub epub) throws Exception {
		epubsExisting.add(new EpubNames(epub));
		return false;
	}

	@Override
	public String getMsgIsNotModified() {
		return "Loaded";
	}

	private byte buffer[] = new byte[16 * 1024 * 1024];

	private void writeTo(ZipOutputStream zo, File f) throws IOException{
		zo.putNextEntry(new ZipEntry(f.getName()));
		FileInputStream fis = new FileInputStream(f);
		int lon = 0;
		while ((lon = fis.read(buffer)) >= 0) {
			zo.write(buffer, 0, lon);
		}
	}
	
	@Override
	public boolean finalizar() throws Exception {
		System.out.print("\nSorting list...");
		Collections.sort(epubsExisting);
		System.out.println("done.");
		int epub2CheckNumber = 0;
		int epub2CheckNewNumber = 0;
		SameLineProgress slp = new SameLineProgress(119, candidateList.size(),"Checking");
		for (File f : candidateList) {
			Epub epub;
			String errMsg = null;
			try {
				epub = new Epub(f);
				EpubNames epubName = new EpubNames(epub);
				int pos = Collections.binarySearch(epubsExisting, epubName);
				if (pos < 0) {
					epub2CheckNewNumber++;
					writeTo(zNewTitles, f);
					epubsExisting.add(-pos - 1, epubName);
				} else {
					if(epubName.dateOfOPF>epubsExisting.elementAt(pos).dateOfOPF){
						epubsExisting.set(pos, epubName);
						writeTo(zNewVersions, f);
					}
				}
			} catch (Exception e) {
				errMsg = e.getMessage();
			}
			epub2CheckNumber++;
			if (errMsg != null) {
				slp.report(epub2CheckNumber, f.getName());
				System.out.println("\n ->Ignoring: " + errMsg);
			} else {
				slp.report(epub2CheckNumber, f.getName(), "News:"
						+ epub2CheckNewNumber + "/" + epub2CheckNumber);
			}
		}
		zNewTitles.close();
		zNewVersions.close();
		return true;
	}

	@Override
	public String getMsgChecking() {
		return "Checking";
	}

	@Override
	public String getMsgIsModified() {
		return "Loaded";
	}

	@Override
	public String getDescription() {
		return "Compare first dir with second one, moving epubs from the second dir to two subdirs of it\n"
				+ "named 'Existing' and 'NonExisting' respectively for epubs yet contained in first dir and\n"
				+ "not contained."
				+ "\nComparison is made in a strange way, so be carefully whith results:"
				+ "\nFirst get lowercase versions of author and title, removing spaces and other signs, "
				+ "\nthen sorting words in both title and author (The black monkey -> black monkey the)"
				+ "\nTwo titles are the same if the resulting string is the same..."
				+ "\nI must remove articles and other commons words, but...";
	}

	class EpubNames implements Comparable<EpubNames> {
		File file;
		String aut;
		String tit;
		long dateOfOPF;
		boolean mark;

		public EpubNames(Epub epub) {
			epub.initComparison();
			file = epub.nameOfFile;
			aut = epub.compareAuthor;
			tit = epub.compareTitle;
			dateOfOPF=epub.dateOfOPF;
		}

		@Override
		public int compareTo(EpubNames o) {
			return 256 * aut.compareTo(o.aut) + tit.compareTo(o.tit);
		}
	}
}
