package xent.checkers;

import java.io.ByteArrayOutputStream;

import xent.epub.Epub;

public class PList2OPF extends Checker {

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	public String getDescription(){
		return "Copy (Author, Title) from iTunesMetadata.plist to OBPS metadata";
	}
	
	public boolean exec(Epub epub) throws Exception{
		boolean toReturn=false;
		
		toReturn|=epub.setAutor_epub(epub.getAutor());
		toReturn|=epub.setTitulo_epub(epub.getTitulo());
		
		if(toReturn){
			String txt=epub.content.getDocAsText();
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			baos.write(txt.getBytes());
			epub.getZFC().put(epub.getOPFLoc(), baos);
		}
		return toReturn;
	}

	@Override
	public String getMsgIsModified() {
		return "OPF changed";
	}

	@Override
	public boolean initialize() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean finalizar() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getMsgChecking() {
		return "iTunes(Auth,Title)->OBPS";
	}

}
