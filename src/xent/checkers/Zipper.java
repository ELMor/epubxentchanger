package xent.checkers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import net.sf.jazzlib.ZipEntry;
import net.sf.jazzlib.ZipOutputStream;
import xent.epub.Epub;

public class Zipper extends Checker {

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	@Override
	public int getNumberOfArgNeeded() {
		return 1;
	};

	static int szMax=256*1024*1024; //bytes max
	int zipNumber=1;
	ZipOutputStream zos=null;
	FileChannel fc=null;
	FileOutputStream fos=null;
	
	/**
	 * 
	 */
	public Zipper(){
		String mx=System.getProperty("zipMaxSizeMB");
		try {
			if(mx!=null)
				szMax=1024*1024*Integer.parseInt(mx);
		} catch (NumberFormatException e) {
			szMax=256*1024*1024;
		}
		if(szMax<32*1024*1024)
			szMax=32*1024*1024;
		System.out.printf("Using zip files of max %dMB\n",szMax/1024/1024);
	}
	
	public String getDescription(){
		return "Create Zip files named 'Zip-%03d.zip' and structured 'AuthFirstLetter/AuthName/BookTitle.epub\n"+
				"Environment variable 'zipMaxSizeMB' set max zip file size. Need Out directory as argument.";
	}
	
	@Override
	public boolean exec(Epub epub) throws Exception {
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		Epub.save(bos,epub.getZFC());
		byte buffer[]=bos.toByteArray();
		if(fc.size()+buffer.length>szMax)
			initialize();
		String nof=normalizeName(epub);
		ZipEntry ze=new ZipEntry(nof);
		zos.putNextEntry(ze);
		zos.write(buffer);
		return false;
	}
	
	public boolean finalizar() throws Exception {
		zos.close();
		return true;
	}

	@Override
	public String getMsgIsModified() {
		return "";
	}
	
	@Override
	public String getMsgIsNotModified(){
		return "Zipped";
	}

	public String normalizeName(Epub epub){
		String au = epub.getAutor();
		String ti = epub.getTitulo();
		String firstLetter=au.trim().substring(0,1).toUpperCase();
		return firstLetter+"/"+au+"/"+au+"-"+ti+".epub";
	}

	private String getNextZipname(){
		return String.format("Zip-%03d.zip", zipNumber++);
	}
	
	@Override
	public boolean initialize() throws Exception {
		if(zos!=null)
			zos.close();
		File odir=new File(argumentos.firstElement(),getNextZipname());
		fos=new FileOutputStream(odir);
		fc=fos.getChannel();
		zos=new ZipOutputStream(fos);
		return true;
	}

	@Override
	public String getMsgChecking() {
		return "Zipping";
	}
	
}
