package xent.checkers;

import xent.epub.Epub;

public class CorregirAutor extends Checker {

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	public String getDescription(){
		return "Make our best setting Author:\n\tName of file can be 'AuthLastname, AuthName - BookTitle.epub'\n"+
				"\tiTunesMetadata.plist metadata has preference over content.obs.";
	}
	
	@Override
	public boolean exec(Epub epub) throws Exception {
		boolean ret=false;
		String autor=epub.getAutor();
		if(autor==null || autor.length()==0){
			autor=epub.getNameOfFile();
			int ndx=autor.indexOf("-");
			if(ndx<0)
				return false;
			autor=autor.substring(ndx+1);
			autor=autor.replace("_", " ");
			autor=autor.trim();
			autor=autor.replace("  ", " ");
			epub.setAutor_epub(autor);
			return true;
		}else{
			if(!epub.getAutor_epub().equals(epub.getAutor_iTunes())){
				epub.setAutor_epub(autor);
				epub.setAutor_iTunes(autor);
				ret=true;
			}
			int ndx=autor.indexOf(",");
			if(ndx<0){
				return ret;
			}else {
				String left=autor.substring(0,ndx);
				String right=autor.substring(ndx+1);
				String definitivo=right.trim()+" "+left.trim();
				epub.setAutor_epub(definitivo);
				epub.setAutor_iTunes(definitivo);
				ret=true;
			}
			return ret;
		}
	}

	@Override
	public String getMsgIsModified() {
		return "Autor corregido";
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean finalizar() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getMsgChecking() {
		return "Checking author";
	}

}
