package xent.epub.model;

import java.util.Hashtable;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

public class NSCtxHelper implements NamespaceContext {

	Hashtable<String, String> fwd=new Hashtable<String, String>();
	Hashtable<String, String> rev=new Hashtable<String, String>();
	
	public void addRef(String ref, String uri){
		fwd.put(ref, uri);
		rev.put(uri, ref);
	}
	
	@Override
	public String getNamespaceURI(String ref) {
		return fwd.get(ref);
	}

	@Override
	public String getPrefix(String uri) {
		return rev.get(uri);
	}

	@Override
	public Iterator<String> getPrefixes(String arg0) {
		return fwd.keySet().iterator();
	}

}
