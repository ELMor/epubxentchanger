package xent.epub.model;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Represents OPF file on an epub (pointed by META-INF/container.xml)
 * @author elinares
 *
 */
public class Content extends Helper{
	Node nAut,nTit,nTOCitem;
	NodeList items;
	String tocId;
	
	public Content(String baos) throws Exception{
		super(baos);
		NSCtxHelper ctx=new NSCtxHelper();
		ctx.addRef("opf","http://www.idpf.org/2007/opf");
		ctx.addRef("dc", "http://purl.org/dc/elements/1.1/");
		xpath.setNamespaceContext(ctx);
		nAut=getNode("/package/metadata/creator");
		nTit=getNode("/package/metadata/title");
		nTOCitem=getNode("/package/spine");
		tocId=nTOCitem.getAttributes().getNamedItem("toc").getTextContent();
		items=getNodeList("/package/manifest/item");
		for(int i=0;i<items.getLength();i++){
			Node n=items.item(i);
			if(tocId.equals(n.getAttributes().getNamedItem("id").getTextContent())){
				nTOCitem=n;
				break;
			}
		}
	}
	
	public boolean setAutor_epub(String aut){
		if(aut.equals(getAutor_epub()))
			return false;
		nAut.setTextContent(aut);
		return true;
	
	}
	
	public boolean setTitulo_epub(String tit){
		if(tit.equals(getTitulo_epub()))
			return false;
		nTit.setTextContent(tit);
		return true;
	}
	
	public String getTitulo_epub(){
		return nTit.getTextContent();
	}
	
	public String getAutor_epub(){
		return nAut.getTextContent();
	}

	public String getTOCRef(){
		return nTOCitem.getAttributes().getNamedItem("href").getTextContent();
	}
}
