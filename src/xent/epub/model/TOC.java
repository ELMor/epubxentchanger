package xent.epub.model;



/**
 * Represents TOC.ncx on an epub for navigation
 * @author elinares
 *
 */
public class TOC extends Helper {
	public TOC(String baos) throws Exception {
		super(baos);
		NSCtxHelper ctx=new NSCtxHelper();
		xpath.setNamespaceContext(ctx);
	}
}
