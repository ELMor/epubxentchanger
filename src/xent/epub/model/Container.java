package xent.epub.model;

import org.w3c.dom.Node;



/**
 * Represents META-INF/container.xml file on an epub
 * @author elinares
 *
 */
public class Container extends Helper{
	public Container(String baos) throws Exception{
		super(baos);
		NSCtxHelper ctx=new NSCtxHelper();
		ctx.addRef("c","urn:oasis:names:tc:opendocument:xmlns:container");
		xpath.setNamespaceContext(ctx);
	}
	public String getOBPSName(){
		Node n=getNode("/container/rootfiles/rootfile");
		return n.getAttributes().getNamedItem("full-path").getTextContent();
	}
}
